describe('pokemon list page', () => {
  it('renders a page with list of pokemons', () => {
    cy.visit('http://localhost:8000');
    cy.get('[data-testid="card"]').first().click();
    cy.get('[data-testid="modla"]').contains('height');
    cy.get('[data-testid="modla"]').get('button').click();
    cy.get('[data-testid="modla"]').should('be.hidden');
  })
})