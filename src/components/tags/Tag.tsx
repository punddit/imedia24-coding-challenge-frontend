import * as React from "react";

const Tag = (props: any) => {
    return (
        <span className="border-1 bg-slate-300 m-1 rounded-full inline-block p-1 px-2 text-slate-700 text-sm">{props.label}</span>
    )
}

export default Tag;