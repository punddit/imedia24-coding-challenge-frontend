import * as React from "react";
interface Props {
    title: React.ReactNode;
    url: string;
    onClick: any;
}


function Card(props: Props) {
    const urlArray = props.url.split("/");
    const id = urlArray[urlArray.length - 2];
    return (
        <div data-testid="card" className="bg-white cursor-pointer w-44 mb-4 justify-between items-center uppercase font-bold shadow-md rounded p-1" onClick={(e) => props.onClick(e, props.url)}>
            <div className="flex items-center justify-center relative w-40 h-40">
                <img className="" src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`} />
            <div className="w-8 text-center bg-slate-200 opacity-80 text-slate-700 rounded-full absolute left-1 top-2">{id}</div>
            </div>
                <div className="text-slate-700 text-center">{props.title}</div>
        </div>
    )
}
export default Card;