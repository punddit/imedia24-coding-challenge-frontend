import * as React from "react";

export interface CardGridProps extends React.HTMLAttributes<HTMLDivElement> {
rows:number,
};

const Grid: React.FC<CardGridProps> = (props) => (<div className={`flex flex-wrap items-center justify-around m-auto`}>{props.children}</div>);

export default Grid;