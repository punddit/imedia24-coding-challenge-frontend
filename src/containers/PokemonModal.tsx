import * as React from "react";
import { CloseButton } from "../components/buttons";
import {Tag,TagList} from "../components/tags";

interface PokeCardProps {
  pokemon: {};
  hidden: boolean;
  setHidden: any;
}



const RoundLabel = (props: any) => {
  return (
    <span className="w-1/2  text-left">
      <span className="block font-bold">{props.label}</span>
      <span className="">{props.content}</span>
    </span>
  );
}

const PokemonDetails = (props: any) => {
  const pokemon = props.pokemon;
  return (
    <div className="flex flex-wrap justify-around items-end mb-4 text-slate-700">
      <h3 className="w-full font-extrabold text-center text-lg uppercase m-2">{pokemon.name}</h3>
      <div>
        <img className="rounded-sm m-auto" src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`} />
      </div>
      <div className="flex flex-wrap justify-around items-center">
        <RoundLabel content={pokemon.height} label="height" />
        <RoundLabel content={pokemon.weight} label="weight" />
        <RoundLabel content={pokemon.order} label="order" />
        <RoundLabel content={pokemon.base_experience} label="base experience" />
      </div>
    </div>
  );
}




export default function PokemonModal(props: PokeCardProps) {
  const pokemon: any = props.pokemon;
  return (
    <>
      <div data-testid="modal" className={` ${props.hidden ? 'hidden' : ''} overflow-y-auto bg-stone-700/50 overflow-x-hidden fixed top-0 right-0 left-0 z-50 md:inset-0 h-full justify-center items-center`}>
        <div className="relative p-4 w-full max-w-md h-full md:h-auto m-auto">
          <div className="relative bg-white rounded-lg shadow ">
            <CloseButton handleClick={() => props.setHidden(true)} />
            <div className="p-6 text-center">
              <PokemonDetails pokemon={pokemon} />
              <TagList>
                {pokemon.abilities && pokemon.abilities.map((ability: any) => <Tag label={ability.ability.name} />)}
              </TagList>
              <TagList>
                {pokemon.stats && pokemon.stats.map((stat: any) => <Tag label={stat.stat.name} />)}
              </TagList>
            </div>
          </div>
        </div>
      </div>
    </>

  );
}