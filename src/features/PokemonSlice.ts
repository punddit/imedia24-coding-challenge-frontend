import { createSlice, PayloadAction } from "@reduxjs/toolkit";


export  const pokemonSlice = createSlice({
    name:'pokemons',
    initialState:{
        pokemons:[],
        selectedPokemon:{},
        offset:0,
        limit:100,
        isLoading:false
    },
    reducers:{
         getPokemonsFetch: (state,action) => {
            state.isLoading = true;
            state.limit = state.limit + action.payload.limit;
         },
         getPokemonsSuccess:(state, action) => {
            state.pokemons = action.payload.results;
            state.isLoading = false;
         },
         getPokemonsFailure:(state) => {
            state.isLoading = false;
         },
         getPokemonFetch: (state,action) => {
            state.isLoading = false;
         },
         getPokemonSuccess:(state, action) => {
            state.selectedPokemon = action.payload;
            state.isLoading = false;
         },
         getPokemonFailure:(state) => {
            state.isLoading = false;
         }
    }
});

export const {getPokemonsFetch,getPokemonsSuccess,getPokemonsFailure,getPokemonFetch,getPokemonSuccess,getPokemonFailure} = pokemonSlice.actions;
export default pokemonSlice.reducer;