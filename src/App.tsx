import * as React from "react";
import { Component } from "react";
import PokemonList from "./containers/PokemonList";

class App extends Component {
  render() {
    return (
      // <div style={{background:'#141b1f'}}>
      <div
        className=" mx-auto bg-slate-50"
        style={{ scrollBehavior: "smooth" }}
      >
        <PokemonList/>
      </div>
    );
  }
}

export default App;
