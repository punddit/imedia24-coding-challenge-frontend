import {configureStore} from "@reduxjs/toolkit";
import pokemonsReducer from './features/PokemonSlice';
import createSagaMiddleware from "@redux-saga/core";
import pokeSaga from "./sagas/pokeSaga";

const saga = createSagaMiddleware();
export const store = configureStore({
    reducer:{
        pokemons:pokemonsReducer,
    },
    middleware:[saga]
});

saga.run(pokeSaga);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;